import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-reactiveforms',
  templateUrl: './reactiveforms.component.html',
  styleUrls: ['./reactiveforms.component.css']
})
export class ReactiveformsComponent {
  data: any = '';

  contactForm = new FormGroup({
    firstname: new FormControl('', Validators.required),
    lastname: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', Validators.required),
    pincode: new FormControl('', [
      Validators.required,
      Validators.min(100000),
      Validators.max(999999),
    ]),
    phone: new FormControl('', [
      Validators.required,
      Validators.min(100000000),
      Validators.max(9999999999),
    ]),
  });

  onSubmit() {
    this.data = this.contactForm.value;
    this.contactForm.reset();
  }
}

