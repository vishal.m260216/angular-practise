import { Component, Injectable, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';

@Injectable()
@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.css'],
})
export class EmployeeDetailComponent implements OnInit {

 public employees:any = [];
  
  constructor(private employeeServices: EmployeeService) { }

  ngOnInit() {
    this.employeeServices.getEmployees().subscribe(res=>{
      this.employees = res;
    })
      
        
  }

}
