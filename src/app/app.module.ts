import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BasicsComponent } from './basics/basics.component';
import { ReactiveformsComponent } from './reactiveforms/reactiveforms.component';
import { EmployeeDetailComponent } from './employee-detail/employee-detail.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { EmployeeService } from './employee.service';


@NgModule({
  declarations: [
    AppComponent,
    BasicsComponent,
    ReactiveformsComponent,
    EmployeeDetailComponent,
    EmployeeListComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [EmployeeService], 
  bootstrap: [AppComponent]
})
export class AppModule { }
