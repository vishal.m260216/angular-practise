import { Component } from '@angular/core';

@Component({
  selector: 'app-basics',
  templateUrl: './basics.component.html',
  styleUrls: ['./basics.component.css']
})
export class BasicsComponent {
  public highlightColor = "orange";
public siteUrl = window.location.href;
public isDisabled = true;
public name = "Vishal";
displayName = true;
public color = "green";
public colors = ["red", "blue", "green", "yellow"]
public person = {
  "firstName" : "John",
  "LastName" : "Doe",
}

public employees = [
  {"id": 1, "name": "Andrew", "age": 30},
  {"id": 2, "name": "Jesoph", "age": 27},
  {"id": 3, "name": "Glen", "age": 24}
]
public date = new Date();

greeting = "";
  constructor() { }
  
greetUser(){
  return 'Hello' + this.name;
}

onClick(event: any){
  console.log(event)
  this.greeting = "welcome to angular";
  this.greeting = event.type;
}
logMessage(value:any){
  console.log(value);
}

}
